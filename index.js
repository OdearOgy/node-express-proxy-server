const path = require('path');
require('dotenv').config({ path: path.join(__dirname, '.env') });

const express = require('express');
const morgan = require("morgan");
const { createProxyMiddleware } = require('http-proxy-middleware');
const app = express();

// Configuration
const PORT = process.env.APP_PORT;
const HOST = process.env.APP_HOST;
const API_SERVICE_URL = process.env.API_SERVICE_URL;
const API_KEY = process.env.API_KEY;
app.use(morgan('dev'));

// app.use(function (req, res, next) {
//   res.header('Access-Control-Allow-Origin', '*')
//   res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
//   next()
// })

app.use('/', createProxyMiddleware({
  target: API_SERVICE_URL,
  changeOrigin: true,
  onProxyReq: function (proxyReq, req, res) {
    proxyReq.setHeader('x-api-key', API_KEY);
  }
}));

app.listen(PORT, HOST, () => {
  console.log(`Starting Proxy at ${HOST}:${PORT}`);
});